alsaequal (0.6-8) unstable; urgency=medium

  * Team upload

  [ Helmut Grohne ]
  * Fix FTCBFS: Force using $(CC) as $(LD). (Closes: #865743)

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Sebastian Ramacher ]
  * debian/rules: Use dpkg's architecture.mk
  * debian/control:
    - Remove obsolete Pre-Depends
    - Bump debhelper compat to 12
    - Bump Standards-Version
  * debian/patches: Fix link order (Closes: #882378)

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 08 Dec 2019 19:12:04 +0100

alsaequal (0.6-7) unstable; urgency=medium

  * Remove myself from the uploaders list.

 -- Alessio Treglia <alessio@debian.org>  Mon, 12 Dec 2016 07:34:48 +0000

alsaequal (0.6-6) unstable; urgency=low

  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Fix Eq CAPS plugin name (Closes: #721355)
  * Add 06_fix-caps-error.patch to fix a spurious error with newer caps versions
  * Tighten Depends on caps
  * Canonicalize Vcs-Git field too

 -- Alessandro Ghedini <ghedo@debian.org>  Fri, 30 Aug 2013 19:21:05 +0200

alsaequal (0.6-5) unstable; urgency=low

  * Install default conf to system-wide alsa-lib config path
    and simplify example (Closes: #685955)

 -- Alessandro Ghedini <ghedo@debian.org>  Tue, 28 Aug 2012 14:44:06 +0200

alsaequal (0.6-4) unstable; urgency=low

  * Disable quiet build so that build flags are visible
  * Enable security hardening flags
    Thanks to Simon Ruderich for the patch (Closes: #662695)
  * Email change: Alessandro Ghedini -> ghedo@debian.org

 -- Alessandro Ghedini <ghedo@debian.org>  Sat, 31 Mar 2012 14:34:05 +0200

alsaequal (0.6-3) unstable; urgency=low

  [ Alessio Treglia ]
  * Fix Vcs-Browser field.

  [ Alessandro Ghedini ]
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3
  * Bump debhelper compat level to 9
  * Remove manual Depends on libasound2
  * Fix conf example path
  * Add Multi-Arch header (and Pre-Depends too)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 26 Feb 2012 14:49:41 +0100

alsaequal (0.6-2) unstable; urgency=low

  * Team upload

  [ Alessio Treglia ]
  * Replace the note with the newest one used by FSF.

  [ Alessandro Ghedini ]
  * Fix copy-paste typo in LGPL note
  * Add missing 'Lesser' in LGPL license
  * Build on Linux only

  [ Reinhard Tartler ]
  * Install into multiarch directories. Closes: #635005
  * Bump standards version (no changes needed)
  * update Vcs-Browser field

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 28 Jul 2011 07:52:32 +0200

alsaequal (0.6-1) unstable; urgency=low

  * Initial release (Closes: #604908).

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 25 Nov 2010 18:28:00 +0100
